﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTC_Challenge_DH.Models
{
	public class FooBar
	{
		public int Number { get; set; }
		public string Value { get; set; }

		/// <summary>
		/// Constructor calculates what it's initialised input is divisible by and stores the relevant output value.
		/// </summary>
		/// <param name="initValue"></param>
		public FooBar(int initValue)
		{
			Number = initValue;
			var multipleOf = Divisor.FindMultipleOf(initValue);

			switch (multipleOf)
			{
				case enMultipleOf.FiveOrSeven:
					Value = "FooBar";
					break;

				case enMultipleOf.Seven:
					Value = "Bar";
					break;
				
				case enMultipleOf.Five:
					Value = "Foo";
					break;
				
				// Save null checks on the client by loading printable values by default
				default:
					Value = Number.ToString();
					break;
			}
		}

	}


	
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTC_Challenge_DH.Models
{
	public class RomanNumeral
	{
		public int Value { get; set; }
		public string Numeral { get; set; }

		public RomanNumeral(int value)
		{
			Value = value;
			Numeral = CalculateNumeral(value);
		}

		private string CalculateNumeral(int inputValue)
		{
			var descNumerals = ConversionTable.Reverse();
			var tempResult = "";

			while (inputValue > 0)
			{
				foreach(var number in descNumerals)
				{
					if (number.Key <= inputValue)
					{
						tempResult += number.Value;
						inputValue -= number.Key;
						break; // Force the sequence to start again
					}
				}
			}
			return tempResult;
		}	

		private static readonly Dictionary<int, string> ConversionTable = new Dictionary<int, string>()
		{	
			{1, "I"},
			{4, "IV"},
			{5, "V"},
			{9, "IX"},
			{10, "X"},
			{40, "XL"},
			{50, "L"},
			{90, "XC"},
			{100, "C"},
			{400, "CD"},
			{500, "D"},
			{900, "CM"},
			{1000, "M"}
		};
	}
}

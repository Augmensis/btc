﻿namespace BTC_Challenge_DH.Models
{
	public enum enMultipleOf
	{
		None = 0,
		Five = 5,
		Seven = 7,
		FiveOrSeven = 35
	}

	public static class Divisor
	{
		/// <summary>
		/// Takes an input Integer and returns what it is divisible by
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public static enMultipleOf FindMultipleOf(int input)
		{
			if(input % 35 == 0) return enMultipleOf.FiveOrSeven;
			if(input % 7 == 0) return enMultipleOf.Seven;
			if(input % 5 == 0) return enMultipleOf.Five;
			return enMultipleOf.None;
		}
	}
}

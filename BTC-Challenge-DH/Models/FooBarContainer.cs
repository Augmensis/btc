﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTC_Challenge_DH.Models
{
	public class FooBarContainer
	{
		public List<FooBar> Results { get; set; }

		/// <summary>
		/// Constructor initialises a list of results containing FooBar objects that manage themselves
		/// </summary>
		/// <param name="numberOfResults"></param>
		public FooBarContainer(int numberOfResults)
		{
			Results = new List<FooBar>();
			for (var i = 1; i < numberOfResults + 1; i++)
			{
				Results.Add(new FooBar(i));
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BTC_Challenge_DH.Models;

namespace BTC_Challenge_DH.Controllers.API
{
    public class FooBarController : ApiController
    {

        // GET: api/FooBarApi/5
        public IEnumerable<FooBar> Get(int numberOfResults = 100)
        {
			var container = new FooBarContainer(numberOfResults);
	        return container.Results;
        }

    }
}

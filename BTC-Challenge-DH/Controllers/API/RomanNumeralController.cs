﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BTC_Challenge_DH.Models;

namespace BTC_Challenge_DH.Controllers.API
{
    public class RomanNumeralController : ApiController
    {
		// GET: api/RomanNumeral/5
        public RomanNumeral Get(int number)
        {
            return new RomanNumeral(number);
        }
    }
}

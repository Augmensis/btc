﻿using System.Web;
using System.Web.Mvc;

namespace BTC_Challenge_DH
{
	public class FilterConfig
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
		}
	}
}

﻿using System.Web;
using System.Web.Optimization;

namespace BTC_Challenge_DH
{
	public class BundleConfig
	{
		// For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles)
		{
			const string appLocation = "~/Content/js/app/";
			bundles.Add(new ScriptBundle("~/bundles/app").Include(
						"~/Scripts/angular.js",
						"~/Scripts/angular-route.js",
						"~/Scripts/angular-sanitize.js",
						appLocation + "app.js",
						appLocation + "/services/foobarService.js",
						appLocation + "/services/romanService.js",
						appLocation + "/controllers/mainController.js",
						appLocation + "/controllers/fooBarController.js",
						appLocation + "/controllers/romanController.js"
						
					));

			bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
						"~/Scripts/jquery-{version}.js"));

			// Use the development version of Modernizr to develop with and learn from. Then, when you're
			// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
			bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
						"~/Scripts/modernizr-*"));

			bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
					  "~/Scripts/bootstrap.js",
					  "~/Scripts/respond.js"));

			bundles.Add(new StyleBundle("~/bundles/css").Include(
					  "~/Content/css/bootstrap.css",
					  "~/Content/css/site.css"));
		}
	}
}

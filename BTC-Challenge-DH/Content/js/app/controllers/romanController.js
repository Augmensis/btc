﻿app.controller("romanController", ["$scope", "romanService", function ($scope, romanService) {
	$scope.result = null;
	$scope.inputNumber = 100;

	$scope.errorMsg = null;

	$scope.go = function () {
		if ($scope.inputNumber <= 0 || $scope.inputNumber > 3999) {
			$scope.errorMsg = "Please select a number between 1 and 3,999";
			return;
		}

		romanService.calculate($scope.inputNumber)
			.then(function (res) {
				$scope.result = res;
			});
	}
}]);
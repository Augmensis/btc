﻿app.controller("fooBarController", ["$scope", "foobarService", function ($scope, foobarService) {
	$scope.results = {};
	$scope.inputNumber = 100;

	$scope.errorMsg = null;

	$scope.go = function () {
		if ($scope.inputNumber <= 0 || $scope.inputNumber > 10000) {
			$scope.errorMsg = "Please select a number between 1 and 10,000";
			return;
		}

		foobarService.fetch($scope.inputNumber)
			.then(function (res) {
				$scope.results = res;
			});
	}
}]);
﻿app.service("foobarService", ["$http", function ($http) {
	var foobarService = {};

	foobarService.fetch = function (numberOfResults) {
		var promise = $http.get("/api/foobar", { params: { numberOfResults: numberOfResults } })
			.then(function(res) {
				return res.data;
			}, function(err) {
				console.log("FooBar Error: " + err);
			});
		return promise;
	};
	
	return foobarService;
}]);
﻿app.service("romanService", ["$http", function ($http) {
	var romanService = {};

	romanService.calculate = function (number) {
		var promise = $http.get("/api/romanNumeral", { params: { number: number } })
			.then(function (res) {
				return res.data;
			}, function (err) {
				console.log("Roman Numeral Error: " + err);
			});
		return promise;
	};

	return romanService;
}]);
﻿var app = angular.module("app", ["ngRoute"]);

app.config(function ($routeProvider) {
	$routeProvider
		.when("/", {
			templateUrl: "content/js/app/views/index.html",
			controller: "mainController"
		})
		.when("/foobar", {
			templateUrl: "content/js/app/views/foobar.html",
			controller: "fooBarController"
		})
		.when("/romanNumerals", {
			templateUrl: "content/js/app/views/romanNumerals.html",
			controller: "romanController"
		});
});


